import { FC } from 'react';
import { Link } from 'react-router-dom';

export const SignUpForm:FC = () => {
    return (
        <section className = 'publish-tip sign-form'>
            <form>
                <fieldset>
                    <legend>Регистрация</legend>
                    <label className = 'label'>
                        <input
                            className = '' lang = ''
                            placeholder = 'Имя и фамилия' type = 'text'
                            name = 'name' />
                    </label>
                    <label className = 'label'>
                        <input
                            className = '' lang = ''
                            placeholder = 'Электропочта' type = 'text'
                            name = 'email' />
                    </label>
                    <label className = 'label'>
                        <input
                            className = '' lang = 'en'
                            placeholder = 'Пароль' type = 'password'
                            name = 'password' />
                    </label>
                    <label className = 'label'>
                        <input
                            className = '' lang = 'en'
                            placeholder = 'Подтверждение пароля' type = 'password'
                            name = 'confirmPassword' />
                    </label>
                    <input
                        className = 'button-login' type = 'submit'
                        value = 'Зарегистрироваться' />
                </fieldset>
                <p>Перейти к <Link to = '/todo/login'>логину</Link>.</p>
            </form>
        </section>
    );
};
