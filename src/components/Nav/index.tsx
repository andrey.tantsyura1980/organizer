import { FC } from 'react';
import { Link } from 'react-router-dom';

export const Nav:FC = () => {
    return (
        <>
            <Link
                to = '/todo/task-manager'
                aria-disabled = 'false' className = ''>К задачам</Link>
            <Link
                to = '/todo/profile'
                aria-disabled = 'false' className = ''>Профиль</Link>
            <button className = 'button-logout'>Выйти</button>
        </>
    );
};
