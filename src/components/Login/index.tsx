import { FC } from 'react';
import { Link } from 'react-router-dom';

export const LoginForm: FC = () => {
    return (
        <section className = 'sign-form'>
            <form>
                <fieldset>
                    <legend>Вход</legend>
                    <label className = 'label'>
                        <input
                            className = '' lang = ''
                            placeholder = 'Электропочта' type = 'text'
                            name = 'email' />
                    </label>
                    <label className = 'label'>
                        <input
                            className = '' lang = 'en'
                            placeholder = 'Пароль' type = 'password'
                            name = 'password' />
                    </label>
                    <input
                        className = 'button-login' type = 'submit'
                        value = 'Войти'></input>
                </fieldset>
                <p>Если у вас до сих пор нет учётной записи, вы можете <Link to = '/todo/signup'>зарегистрироваться</Link>.</p>
            </form>
        </section>
    );
};
