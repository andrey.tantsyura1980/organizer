// Core
import { FC } from 'react';
import { Route, Router } from 'react-router-dom';

// Components
import {
    Nav,
    Footer,
} from './components';


// Instruments


export const App: FC = () => {
    return (
        <>
            <nav>
                <Nav />
            </nav>
            <main>
                <Footer />
            </main>
        </>
    );
};

